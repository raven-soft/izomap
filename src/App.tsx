import 'bootstrap/dist/css/bootstrap-grid.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'leaflet/dist/leaflet.css';
import React from 'react';
import './App.css';
import Controls from './component/Controls';
import Map from './component/Map';
export default class App extends React.Component<any, any> {

  render() {
    return (
      <div>
        <Controls />
        <Map />
      </div>
    );
  }
}

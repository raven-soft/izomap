import * as bootstrap from 'bootstrap';
import React from 'react';
import { connect } from 'react-redux';
import {
  changeCalcType,
  changeIntervalValue,
  changeReachValue,
  changeTransportMode,
  clearIzochrones,
  downloadIzochrones
} from '../state/actions';
import { AppState } from '../state/reducers';
import './Controls.css';
class Controls extends React.Component<any, any> {
  private collapseMenu;

  componentDidMount() {
    new bootstrap.Collapse(this.collapseMenu);
  }

  handleTransportModeChange(
    event: React.ChangeEvent<{ name?: string; value: unknown }>
  ) {
    this.props.changeTransportMode(event.target.value);
  }

  handleCalcTypeChange(
    event: React.ChangeEvent<{ name?: string; value: unknown }>
  ) {
    this.props.changeCalcType(event.target.value);
  }

  handleReachValueChange(
    event: React.ChangeEvent<{ name?: string; value: number }>
  ) {
    this.props.changeReachValue(event.target.value);
  }

  handleIntervalValueChange(
    event: React.ChangeEvent<{ name?: string; value: number }>
  ) {
    this.props.changeIntervalValue(event.target.value);
  }

  render() {
    return (
      <>
        <div className="accordion accordion-flush" id="accordionFlushExample">
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingOne">
              <button
                className="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#flush-collapseOne"
                aria-expanded="false"
                aria-controls="flush-collapseOne">
                Ustawienia
              </button>
            </h2>
            <div
              ref={collapseMenu => (this.collapseMenu = collapseMenu)}
              id="flush-collapseOne"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingOne"
              data-bs-parent="#accordionFlushExample">
              <div className="accordion-body card card-body controls-card">
                <div className="row">
                  <form className="col-12 col-lg-6">
                    <div className="row mb-3">
                      <label
                        htmlFor="izochroneType"
                        className="col-6 col-form-label">
                        Typ izochrony
                      </label>
                      <div className="col-6">
                        <select
                          id="izochroneType"
                          name="izochroneType"
                          className="form-select"
                          value={this.props.transportMode}
                          onChange={this.handleTransportModeChange.bind(this)}>
                          <option value={'foot-walking'}>Pieszy</option>
                          <option value={'cycling-regular'}>Rower</option>
                          <option value={'driving-car'}>Samochód</option>
                        </select>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="calcType"
                        className="col-6 col-form-label">
                        Na podstawie
                      </label>
                      <div className="col-6">
                        <select
                          id="calcType"
                          name="calcType"
                          className="form-select"
                          value={this.props.calcType}
                          onChange={this.handleCalcTypeChange.bind(this)}>
                          <option value={'time'}>Czasu</option>
                          <option value={'distance'}>Odleglości</option>
                        </select>
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="reachVal"
                        className="col-6 col-form-label">
                        Zasięg{' '}
                        {this.props.calcType === 'time' ? '(min)' : '(m)'}
                      </label>
                      <div className="col-6">
                        <input
                          id="reachVal"
                          className="form-control"
                          name="reachVal"
                          type="number"
                          value={this.props.reachValue}
                          onChange={this.handleReachValueChange.bind(this)}
                        />
                      </div>
                    </div>
                    <div className="row mb-3">
                      <label
                        htmlFor="interval"
                        className="col-6 col-form-label">
                        Interwal{' '}
                        {this.props.calcType === 'time' ? '(min)' : '(m)'}
                      </label>
                      <div className="col-6">
                        <input
                          id="interval"
                          className="form-control"
                          name="interval"
                          type="number"
                          value={this.props.intervalValue}
                          onChange={this.handleIntervalValueChange.bind(this)}
                        />
                      </div>
                    </div>
                  </form>
                  <div className="col-12 col-lg-6">
                    <div className="mb-3">
                      <button
                        className="btn btn-sm btn-outline-primary"
                        onClick={this.props.clearIzochrones}>
                        Wyczyść izochrony
                      </button>
                    </div>
                    <div className="mb-3">
                      <button
                        className="btn btn-sm btn-outline-primary"
                        onClick={this.props.downloadIzochrones}>
                        Pobierz izochrony
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  transportMode: state.transportMode,
  calcType: state.calcType,
  reachValue: state.reachValue,
  intervalValue: state.intervalValue,
});

export default connect(mapStateToProps, {
  changeTransportMode,
  changeIntervalValue,
  changeReachValue,
  changeCalcType,
  clearIzochrones,
  downloadIzochrones,
})(Controls);

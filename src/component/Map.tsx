import L, { LatLng } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import React from 'react';
import { connect } from 'react-redux';
import { addMarker } from '../state/actions';
import { AppState } from '../state/reducers';

class Map extends React.Component<any, any> {
  map: L.Map;
  markerLayer: L.LayerGroup;
  accessLayer: L.GeoJSON;

  componentDidUpdate(prevProps: Readonly<any>) {
    console.log('comp update', this.props.markers);
    const icon = L.icon({
      iconUrl: process.env.PUBLIC_URL + '/location_on-24px.svg',
      iconSize: [24, 24],
      iconAnchor: [12, 22],
    });
    if (this.props.markers !== prevProps.markers) {
      this.markerLayer.clearLayers();
      this.props.markers.forEach(m => {
        this.markerLayer.addLayer(
          L.marker(new LatLng(m[1], m[0])).setIcon(icon)
        );
      });
    }
    if (this.props.mergedIzochrones !== prevProps.mergedIzochrones) {
      this.accessLayer.clearLayers();
      this.accessLayer.addData(this.props.mergedIzochrones);
    }
  }

  componentDidMount() {
    this.map = L.map('izomap').setView([51.108855, 17.027542], 12);
    this.accessLayer = L.geoJSON().addTo(this.map);
    this.markerLayer = L.layerGroup().addTo(this.map);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.map);
    this.map.on('click', (e: any) => {
      const latLng = e.latlng;
      console.log('latLng', latLng);
      this.props.addMarker(
        [latLng.lng, latLng.lat],
        this.props.transportation,
        this.props.sliderPoints
      );
    });
  }

  render() {
    return <div id="izomap" />;
  }
}

const mapStateToProps = (state: AppState) => ({
  markers: state.markers,
  mergedIzochrones: state.mergedIzochrones,
});

export default connect(mapStateToProps, { addMarker })(Map);

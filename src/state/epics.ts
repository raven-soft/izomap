import { Feature, Polygon, union } from '@turf/turf';
import Axios from 'axios';
import { combineEpics } from 'redux-observable';
import { Observable } from 'rxjs';
import { filter, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import {
  Action,
  ADD_MARKER,
  DOWNLOAD_IZOCHRONES,
  IzochroneCreatedAction,
  IzochronesMergedAction,
  IZOCHRONES_MERGED,
  IZOCHRONE_CREATED
} from './actions';
import { AppState } from './reducers';
const _ = require('lodash');

const axios = Axios.create({
  headers: {
    Authorization: '5b3ce3597851110001cf6248b03abfde67094d95a29ace47683ac5a3',
  },
});

export const addMarkerEpic = (
  action$: Observable<any>,
  state$: Observable<AppState>
): Observable<IzochroneCreatedAction> => {
  return action$.pipe(
    filter(action => action.type === ADD_MARKER),
    withLatestFrom(state$),
    switchMap(([action, state]) => {
      console.log('state', state);
      return axios
        .post(
          `https://api.openrouteservice.org/v2/isochrones/${state.transportMode}`,
          {
            locations: [action.payload.lnlt],
            range: [
              calculateValueBasedOnType(state.reachValue, state.calcType),
            ],
            attributes: ['reachfactor'],
            range_type: state.calcType,
            interval: calculateValueBasedOnType(
              state.intervalValue,
              state.calcType
            ),
          }
        )
        .catch(err => null);
    }),
    filter(response => !!response),
    map(response => ({ type: IZOCHRONE_CREATED, payload: response.data }))
  );
};

export const mergeIzochronesEpic = (
  action$: Observable<any>,
  state$: Observable<any>
): Observable<IzochronesMergedAction> => {
  return action$.pipe(
    filter(action => action.type === IZOCHRONE_CREATED),
    withLatestFrom(state$),
    map(([action, { izochrones }]) => {
      console.log('izochrones', izochrones);
      const features = izochrones
        .map(izo => izo.features)
        .reduce((acc, cur) => [...acc, ...cur]);
      console.log('features', features);
      const groupedByDistance = (_.groupBy(
        features,
        (feature: any) => feature.properties.value
      ) as any) as { number: Feature<Polygon>[] };
      console.log('groupedByDistance', groupedByDistance);
      const featureUnions = Object.values(
        groupedByDistance
      ).map((features: Feature<Polygon>[]) => union(...features));
      console.log('featureUnions', featureUnions);
      return featureUnions;
    }),
    map(featureUnions => ({
      type: IZOCHRONES_MERGED,
      payload: { 
        type: "FeatureCollection",
        features: featureUnions },
    }))
  );
};

export const downloadIzochronesEpic = (
  action$: Observable<any>,
  state$: Observable<any>
): Observable<Action<'NOOP', any>> => {
  return action$.pipe(
    filter(action => action.type === DOWNLOAD_IZOCHRONES),
    withLatestFrom(state$),
    tap(([, state]) => {
      console.log(state.mergedIzochrones);
      const file = new Blob([JSON.stringify(state.mergedIzochrones)], { type: "text/plain;charset=utf-8" });
      const a = document.createElement('a');
      a.href = URL.createObjectURL(file);
      a.download = 'izochrones.json';
      a.click();
    }),
    map(() => ({
      type: 'NOOP',
      payload: undefined
    }))
  );
};

function calculateValueBasedOnType(value: number, type: string): number {
  if (type === 'time') {
    return value * 60;
  }
  return value;
}

export default combineEpics(
  addMarkerEpic,
  mergeIzochronesEpic,
  downloadIzochronesEpic
);

export const ADD_MARKER = 'ADD_MARKER';
export const IZOCHRONE_CREATED = 'IZOCHRONE_CREATED';
export const IZOCHRONES_MERGED = 'IZOCHRONES_MERGED';
export const CLEAR_IZOCHRONES = 'CLEAR_IZOCHRONES';
export const DOWNLOAD_IZOCHRONES = 'DOWNLOAD_IZOCHRONES';
export const CHANGE_SLIDER_POINTS = 'CHANGE_SLIDER_POINTS';
export const CHANGE_TRANSPORT_MODE = 'CHANGE_TRANSPORT_MODE';
export const CHANGE_CALC_TYPE = 'CHANGE_CALC_TYPE';
export const CHANGE_REACH_VAL = 'CHANGE_REACH_VAL';
export const CHANGE_INTERVAL_VAL = 'CHANGE_INTERVAL_VAL';

export interface Action<T, P> {
  type: T;
  payload: P;
}
export interface IzochroneCreatedAction {
  type: typeof IZOCHRONE_CREATED;
  payload: any;
}

export interface IzochronesMergedAction {
  type: typeof IZOCHRONES_MERGED;
  payload: any;
}

export interface ChangeSliderPointsdAction {
  type: typeof CHANGE_SLIDER_POINTS;
  payload: any;
}

export interface ClearIzochronesAction {
  type: typeof CLEAR_IZOCHRONES;
}

export interface DownloadIzochronesAction {
  type: typeof DOWNLOAD_IZOCHRONES;
}

export function clearIzochrones(): ClearIzochronesAction {
  return {
    type: CLEAR_IZOCHRONES
  }
}

export function downloadIzochrones(): DownloadIzochronesAction {
  return {
    type: DOWNLOAD_IZOCHRONES
  }
}

export function changeTransportMode(transportMode: string): Action<typeof CHANGE_TRANSPORT_MODE, string>{
  return {
    type: 'CHANGE_TRANSPORT_MODE',
    payload: transportMode
  }
}

export function changeCalcType(calcType: string): Action<typeof CHANGE_CALC_TYPE, string> {
  return {
    type: CHANGE_CALC_TYPE,
    payload: calcType,
  };
}

export function changeReachValue(reachValue: number): Action<typeof CHANGE_REACH_VAL, number> {
  return {
    type: CHANGE_REACH_VAL,
    payload: reachValue,
  };
}

export function changeIntervalValue(intervalValue: number): Action<typeof CHANGE_INTERVAL_VAL, number> {
  return {
    type: CHANGE_INTERVAL_VAL,
    payload: intervalValue,
  };
}

export function addMarker(  lnlt: any[],  transportation: string,  sliderPoints: any[]): Action<typeof ADD_MARKER, any> {
  return {
    type: ADD_MARKER,
    payload: {
      lnlt: lnlt,
      transportation: transportation,
      sliderPoints: sliderPoints,
    },
  };
}

export function ChangeSliderPoints(lnlt: any[]): ChangeSliderPointsdAction {
  return {
    type: CHANGE_SLIDER_POINTS,
    payload: lnlt,
  };
}

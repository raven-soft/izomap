import {
  ADD_MARKER,
  CHANGE_CALC_TYPE,
  CHANGE_INTERVAL_VAL,
  CHANGE_REACH_VAL,
  CHANGE_SLIDER_POINTS,
  CHANGE_TRANSPORT_MODE,
  CLEAR_IZOCHRONES,
  IZOCHRONES_MERGED,
  IZOCHRONE_CREATED,
} from './actions';

export interface AppState {
  markers: any[];
  izochrones: any[];
  mergedIzochrones: {};
  sliderPoints: any[];
  transportMode: string;
  calcType: string;
  reachValue: number;
  intervalValue: number;
}

const initialState = {
  markers: [],
  izochrones: [],
  sliderPoints: [100, 200, 700, 900],
  transportMode: 'foot-walking',
  calcType: 'time',
  reachValue: 15,
  intervalValue: 5,
} as AppState;

export const markerReducer = (state = initialState, action: any): AppState => {
  switch (action.type) {
    case CHANGE_SLIDER_POINTS:
      return { ...state, sliderPoints: action.payload };
    case CHANGE_TRANSPORT_MODE:
      return { ...state, transportMode: action.payload };
    case CHANGE_CALC_TYPE:
      return { ...state, calcType: action.payload };
    case CHANGE_REACH_VAL:
      return { ...state, reachValue: action.payload };
    case CHANGE_INTERVAL_VAL:
      return { ...state, intervalValue: action.payload };
    case ADD_MARKER:
      return { ...state, markers: [...state.markers, action.payload.lnlt] };
    case IZOCHRONE_CREATED:
      return { ...state, izochrones: [...state.izochrones, action.payload] };
    case IZOCHRONES_MERGED:
      return { ...state, mergedIzochrones: action.payload };
    case CLEAR_IZOCHRONES:
      return { ...state, markers: [], izochrones: [], mergedIzochrones: [] };
    default:
      return state;
  }
};

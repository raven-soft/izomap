import { applyMiddleware, createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import allEpics from './epics';
import { markerReducer } from './reducers';

const epicMiddleware = createEpicMiddleware();
const appStore = createStore(markerReducer, applyMiddleware(epicMiddleware));
epicMiddleware.run(allEpics)

export default appStore;
